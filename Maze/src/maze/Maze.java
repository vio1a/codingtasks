package maze;

public class Maze {

	private String[][][] construction;
	private int[] startPostition;
	private int maxLevel;
	private int maxRow;
	private int maxColumn;

	public Maze(String[][][] construction, int maxLevel, int maxRow, int maxColumn, int[] startPosition) {
		this.construction = construction;
		this.setMaxLevel(maxLevel);
		this.setMaxRow(maxRow);
		this.setMaxColumn(maxColumn);
		this.setStartPostition(startPosition);
	}

	public int[] getStartPostition() {
		return startPostition;
	}

	public void setStartPostition(int[] startPostition) {
		this.startPostition = startPostition;
	}

	public String[][][] getConstruction() {
		return construction;
	}

	public void setConstruction(String[][][] construction) {
		this.construction = construction;
	}

	public String getField(int x, int y, int z) {
		return this.construction[x][y][z];
	}

	public boolean findWay(int level, int row, int column, int steps, String cameFromDirection) {

		if (getField(level, row, column).equals("E")) {
			return true;
		}

		if (steps == 11) {
			return false;
		}
		
		String field;

		// check one step east
		if (column < maxColumn - 1 && !cameFromDirection.equals("east") && ((field = getField(level, row, column + 1)).equals(".") || field.equals("E"))) {
			if(findWay(level, row, column + 1, steps + 1, "west")){
				return true;
			}
		}

		// check one step south
		if (row < maxRow - 1 && !cameFromDirection.equals("south") && ((field = getField(level, row + 1, column)).equals(".") || field.equals("E"))) {
			if(findWay(level, row + 1, column, steps + 1, "north")){
				return true;
			}
		}

		// check one step west
		if (column > 0 && !cameFromDirection.equals("west") && ((field = getField(level, row, column - 1)).equals(".") || field.equals("E"))) {
			if(findWay(level, row, column - 1, steps + 1, "east")){
				return true;
			}
		}

		// check one step north
		if (row > 0 && !cameFromDirection.equals("north") && ((field = getField(level, row - 1, column)).equals(".") || field.equals("E"))) {
			if(findWay(level, row - 1, column, steps + 1, "south")){
				return true;
			}
		}

		// check one step down
		if (level < maxLevel - 1 && !cameFromDirection.equals("down") && ((field = getField(level + 1, row, column)).equals(".") || field.equals("E"))) {
			if(findWay(level + 1, row, column, steps + 1, "up")){
				return true;
			}
		}

		// check one step up
		if (level > 0 && !cameFromDirection.equals("up") && ((field = getField(level - 1, row, column)).equals(".") || field.equals("E"))) {
			if(findWay(level - 1, row, column, steps + 1, "down")){
				return true;
			}
		}

		return false;
	}
	

	public int getMaxLevel() {
		return maxLevel;
	}

	public void setMaxLevel(int maxLevel) {
		this.maxLevel = maxLevel;
	}

	public int getMaxRow() {
		return maxRow;
	}

	public void setMaxRow(int maxRow) {
		this.maxRow = maxRow;
	}

	public int getMaxColumn() {
		return maxColumn;
	}

	public void setMaxColumn(int maxColumn) {
		this.maxColumn = maxColumn;
	}

}
