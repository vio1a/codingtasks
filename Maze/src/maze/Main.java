package maze;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		// get mazes
		System.out.println("Please enter the path to your maze description file: ");
		Scanner scanner = new Scanner(System.in);
		String filepath = scanner.next();
		scanner.close();

		ArrayList<Maze> mazes = initializeMazes(filepath);

		// for every maze: check way
		for (Maze maze : mazes) {
			boolean result = maze.findWay(maze.getStartPostition()[0], maze.getStartPostition()[1], maze.getStartPostition()[2], 0, "");
			if (result) {
				System.out.println("Entkommen in 11 Minute(n)!");
			} else {
				System.out.println("Gefangen :-(");
			}
		}
	}

	public static ArrayList<Maze> initializeMazes(String filepath) {
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(filepath);
		} catch (FileNotFoundException e) {
			System.out.println("File not found. Please check the path.");
			System.exit(0);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		ArrayList<Maze> mazes = new ArrayList<Maze>();

		try {
			while ((line = br.readLine()) != null && !line.equals("0 0 0")) {

				int levels;
				int rows;
				int columns;

				String[] result = line.split(" ");
				levels = Integer.parseInt(result[0]);
				rows = Integer.parseInt(result[1]);
				columns = Integer.parseInt(result[2]);
				String[][][] construction = new String[levels][rows][columns];
				int[] startPosition = new int[3];

				for (int levelIndex = 0; levelIndex < levels; levelIndex++) {
					
					int rowIndex = 0;

					while ((line = br.readLine()) != null && rowIndex < rows) {

						for (int columnIndex = 0; columnIndex < columns; columnIndex++) {

							String value = Character.toString(line.charAt(columnIndex));

							if (!value.equals("#") && !value.equals(".") && !value.equals("S") && !value.equals("E")) {
								System.out.println("Invalid input value");
								System.exit(0);
							}
							construction[levelIndex][rowIndex][columnIndex] = value;
							if (value.equals("S")) {
								startPosition[0] = levelIndex;
								startPosition[1] = rowIndex;
								startPosition[2] = columnIndex;
							}
						}					
						rowIndex++;
					}
				}
				
				Maze maze = new Maze(construction, levels, rows, columns, startPosition);
				mazes.add(maze);
			}
			
		} catch (NumberFormatException e) {
			System.out.println("Invalid input value");
		} catch (IOException e) {
			System.out.println("Whoops, an error occurred.");
		} catch (Exception e) {
			System.out.println("An error occurred. Please check the validity of the input file.");
		};

		return mazes;
	}
}
