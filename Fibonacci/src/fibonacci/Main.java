package fibonacci;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {

		System.out.println("Please enter the path to your input txt file: ");
		Scanner scanner = new Scanner(System.in);
		String filepath = scanner.next();
		scanner.close();
		
		printFibonacciNumbers(filepath);
	}

	private static double getFibonacciNumber(int number) {
		if(number == 0){
			return 0;
		}
		if(number == 1){
			return 1;
		}
		double fibonacciNumber;
		double fibonacciMinus1 = 1;
		double fibonacciMinus2 = 0;
		for(int i = 2; i <= number; i++){
			fibonacciNumber = fibonacciMinus1 + fibonacciMinus2;
			fibonacciMinus2 = fibonacciMinus1;
			fibonacciMinus1 = fibonacciNumber;
		}
		return fibonacciMinus1;
	}

	private static void printFibonacciNumbers(String filepath) {
		FileInputStream fstream = null;
		try {
			fstream = new FileInputStream(filepath);
		} catch (FileNotFoundException e) {
			System.out.println("File not found. Please check the path.");
			System.exit(0);
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));
		String line;
		int number = 0;
		try {
			while ((line = br.readLine()) != null) {
				try{
					number = Integer.parseInt(line);
					if(number < 0 || number > 5000){
						System.out.println("Invalid input value");
						continue;
					}
				} catch (NumberFormatException e) {
					System.out.println("Invalid input value");
					continue;
				}

				System.out.println("Die Fibonacci Zahl f�r " + number + " ist: " + getFibonacciNumber(number));
			}
		} catch (IOException e) {
			System.out.println("Whoops, an error occurred.");
		}	
		return;
	}

}
